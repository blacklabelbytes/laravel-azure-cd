# Laravel Azure Continuous Deployment

Project is a basic laravel app that should be deployable to azure

# Resource group

Attempted both app service and appservice+db deployment.

When trying the F1 (free, smallest) compute unit, there was no "slots" available: Upgrade to a standard or premium for that.

# Network

The private network connection requires a production setup. In dev/test this is disabled.
When creating DB and app separately, must also add a private network configuration.

Mysql db gets a private network by default when not selecting public IP.
Need to add a separate address space since "default" is occupied (10.0.0.0/24).
Created the 192.168.0.0/24 as "other" which then starts working.

# Steps taken
- Installed a fresh laravel project with breeze and sail + a known user seeded
- Created a PHP web app + Mysql database combined (V3) in azure. This did not have the option of a dev compute unit (0.7GB), only production (2GB+)
- Add Bitbucket repository as source, with direct production deploy to master. Note that real deploys should use slots to CD to staging and then "swap" slots.
- Add application settings (Env vars), advanced export is in production.env.json
- Add a .htaccess rewrite so the default azure php Apache container finds the correct document root.
- Add wildcard to trustproxies middleware so links turn into https
- Force https in azure ssl settings

## Database
- Created at the same time, but is a flexible Mysql server by azure
- A random user is created from start, check resource => mysql server for details
- Create the app database via web ui

Now the database uses SSL by default.
Found a guide: https://docs.microsoft.com/en-us/azure/mysql/flexible-server/tutorial-php-database-app
That suggests downloading the cert: https://dl.cacerts.digicert.com/DigiCertGlobalRootCA.crt.pem
That does not work.
Instead download this: https://www.digicert.com/CACerts/DigiCertGlobalRootCA.crt
Modify extension to .cer
Then upload it as public cert via web ui (public).
The cert will be at /var/ssl/certs with the value in "fingerprint" as the name and a .der (yes a "d") extension.
This value should be added as an application setting "MYSQL_SSL_KEY" with the value.
e.g. "/var/ssl/certs/A8985D3A65E5E5C4B2D7D66D40C6DD2FB19C5436.der"
- Initial migrate/seed via web ssh


# Logging
Custom logs?

# Test
Login should add a log line, to test logging setup

# Observations
- As of 18.2.2022 azure still does not support composer in their php8 containers. This means that the 7.4 container with laravel 8 is the newest possible solution.
This bug has been noted since september 2021, but the minimal change does not seem to be of any priority.
